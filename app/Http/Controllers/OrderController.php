<?php

namespace App\Http\Controllers;
use DB;
class OrderController extends Controller
{
    public function getOrdersById($id)
    {
//
    }

    public function getAllOrders()
    {
        $userId = auth()->user()->id;
        $order = DB::table('tbl_member_store_orders')->where('customer_id', $userId)->get();
        return response()->json(['status' => 'success', 'orders' => $order], 200);
    }
}
