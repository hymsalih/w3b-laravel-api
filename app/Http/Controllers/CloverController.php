<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class CloverController extends Controller
{
    //webhooks
    public function webhooks(Request $request)
    {
        $data = $request->getContent();
        file_put_contents(public_path('/clover-webhooks.txt'), $data, FILE_APPEND);
    }

    //Gets a list of orders
    public function getOrders()
    {
        $token = '80014ff0-f951-a4dd-7b21-9ae02dcdfd88';
        $merchant_id = '14D9D6MF9P5V1';
        $endPoint = $merchant_id . '/orders';
            $response = \App\Clover::cloverApi($endPoint, '', 'GET', $token);
        $response = json_decode($response);
        print_r($response);
    }

    //Create an inventory item
    public function createInventory()
    {
        $token = '80014ff0-f951-a4dd-7b21-9ae02dcdfd88';
        $merchant_id = '14D9D6MF9P5V1';
        $endPoint = $merchant_id . '/items';
        $postData = array(
            "id" => "",
            "hidden" => "",
            "name" => "",
            "alternateName" => "",
            "code" => "",
            "sku" => "",
            "price" => "",
            "priceType" => "",
            "defaultTaxRates" => "",
            "unitName" => "",
            "cost" => "",
            "cost" => "",
            "isRevenue" => "",
            "itemStock" => array(
                "item" => array(
                    "id" => "",
                    "quantity" => "",
                    "modifiedTime" => ""
                )
            ),
        );
        $response = \App\Clover::cloverApi($endPoint, $postData, 'POST', $token);
        $response = json_decode($response);
        print_r($response);
    }

    public function getInventories()
    {
        $token = '80014ff0-f951-a4dd-7b21-9ae02dcdfd88';
        $merchant_id = '14D9D6MF9P5V1';
        $endPoint = $merchant_id . '/items';
        $response = \App\Clover::cloverApi($endPoint, null, 'GET', $token);
        $response = json_decode($response);
        print_r($response);
    }

    public function OAuthCallback(Request $request)
    {
        $merchant_id = $request->merchant_id;
        $employee_id = $request->employee_id;
        $client_id = $request->client_id;
        $code = $request->code;
        $authorized_url = \App\Clover::SANDBOX_AUTHORIZED_MERCHANT_URL . $code;
        try {
            $client = new Client();
            $response = $client->get($authorized_url);
            $responseBody = json_decode((string)$response->getBody(true));
            echo "Your access token :- " . $responseBody->access_token;
        } catch (\Exception $exception) {
            $exception = json_decode((string)$exception->getResponse()->getBody(true));
            echo "Error :- " . $exception->message;
        }
    }

    public function OAuth(Request $request)
    {
        return redirect()->away(\App\Clover::SANDBOX_AUTH_URL . \App\Clover::SANDBOX_CLIENT_ID);
    }
}
