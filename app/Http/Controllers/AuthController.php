<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use DB;
use Hash;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function w3bStoreLogin(Request $request)
    {
        if (auth()->user()->id) {
            $user = DB::table('tbl_customer')->where('email', auth()->user()->email)->first();
            if ($user) {
                $cookie_file = "cookie.txt";
                $tmpfname = dirname(__FILE__) . '/' . $cookie_file;
                $data = array(
                    "login" => "aws3@yahoo.com",
                    "password" => "11111111",
                    "btype" => "1",
                    "password1" => "Password"
                );
                $url = "https://w3bstore.com/market.php/security/login";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//                    "Content-Type: application/x-www-form-urlencoded",
                    "Origin: https://w3bstore.com",
                    "Referer: https://w3bstore.com/market.php/security",
                    "Upgrade-Insecure-Requests: 1",
                    "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36"
                ));
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_COOKIEJAR, $tmpfname);
                curl_setopt($ch, CURLOPT_COOKIEFILE, $tmpfname);
                curl_setopt($ch, CURLOPT_VERBOSE, true);
                $page = curl_exec($ch);
                $err = curl_error($ch);
                $response = curl_getinfo($ch);
                $response['url'] = "https://w3bstore.com/market.php/manage_store/account";
                if ($err) {
                    return response()->json([
                        'status' => 'error',
                        'message' => $err
                    ], 401);
                } else {
                    if ($response['http_code'] == 200) {
                        return response()->json([
                            'status' => 'success',
                            'message' => $err,
                            'url' => $response['url']
                        ], 200);
                    } else {
                        return response()->json([
                            'status' => 'error',
                            'message' => 'Unauthorized'
                        ], 401);
                    }
                }
            }
        }
    }

    public function revokeToken(Request $request)
    {
        $bodyContent = $request->getContent();
        $bodyContent = json_decode($bodyContent);
        $deleted = \App\OAuthAccessToken::where('user_id', auth::user()->id)->where('name', $bodyContent->client)->delete();
        if ($deleted) {
            return response()->json([
                'status' => 'success',
                'message' => 'Token Revoked'
            ], 200);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Token not Revoked'
            ], 401);
        }
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function wordpressAuth(Request $request)
    {
//        $u = DB::table('tbl_api_users')->where('email', 'owner@w3bstore.com')->get();
//        print_r($u);
//        die;
        if (!\App\Helper::apiClientsValidate($request->client)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Invalid clients'
            ]);
        }
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        $activeUser = DB::table('tbl_customer')->select('*')->where('email', $request->email)->where('user_type', 2)->where('locked', 0)->count();
        $activeUser = DB::table('tbl_customer')->select('*')->where('email', $request->email)->get();
        if (count($activeUser) <= 0 || !Auth::attempt($credentials))
            return response()->json([
                'status' => 'error',
                'message' => 'Unauthorized'
            ], 401);
        $user = $request->user();
        $tokenResult = $user->createToken($request->client);
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addYear(100);
        $token->save();
        return response()->json([
            'status' => 'success',
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }
}
