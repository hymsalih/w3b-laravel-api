<?php

namespace App\Http\Controllers;
use DB;
class MenuController extends Controller
{
    public function activeMenu()
    {
        // the menu from w3bstore database
        $menus = DB::table('tbl_kb_menu_bar')->orderBy('bar_name', 'asc')->get();
        return response()->json(['status' => 'success', 'menus' => $menus], 200);
    }
}
