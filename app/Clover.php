<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clover extends Model
{
    //http://69.60.124.194/clover/callback?merchant_id=14D9D6MF9P5V1&employee_id=KR00SPK4YFANY&client_id=3QH1Z07ZT6J80&code=a6c04bb2-e98f-e1d7-e12e-7b356937df12
    /*
     * United States: https://api.clover.com
     * Europe: https://api.eu.clover.com
     */
    //sandbox
    const SANDBOX_AUTHORIZED_MERCHANT_URL = 'https://sandbox.dev.clover.com/oauth/token?client_id=' . \App\Clover::SANDBOX_CLIENT_ID . '&client_secret=' . \App\Clover::SANDBOX_APP_SECRET . '&code=';
    const SANDBOX_AUTH_URL = 'https://sandbox.dev.clover.com/oauth/authorize?client_id=';
    const SANDBOX_BASE_URL = 'https://apisandbox.dev.clover.com/v3/merchants/';
    const SANDBOX_APP_SECRET = 'ca36437a-3d7b-d152-2bec-2cbf84a06e8b';
    const SANDBOX_CLIENT_ID = '3QH1Z07ZT6J80';

    public static function cloverApi($endPoint, $postData, $method, $token)
    {
        $headers = array(
            'Authorization' => 'Bearer ' . $token,
            'content-type' => 'application/json',
        );
        $client = new \GuzzleHttp\Client();
        $options = [
            'headers' => $headers,
            'json' => $postData
        ];
        try {
            if ($method == "POST") {
                $response = $client->post(self::SANDBOX_BASE_URL . $endPoint, $options);
            } else if ($method == "GET") {
                $response = $client->get(self::SANDBOX_BASE_URL . $endPoint, $options);
            }
            return (string)$response->getBody();
        } catch (\Exception $exception) {
            return (string)$exception->getResponse()->getBody(true);
        }
    }
}
