<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Helper extends Model
{

    public static function apiClientsValidate($client)
    {
        if (!in_array($client, array("wordpress-app"))) {
            return false;
        }
        return true;
    }

    function is_json($string, $return_data = false)
    {
        $data = json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
    }

    public static function inValidJson()
    {


        return response()->json([
            'status' => 'error',
            'msg' => 'Invalid json format',
        ]);
    }
}
