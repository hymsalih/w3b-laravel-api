<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//clover api
Route::get('/clover/callback', 'CloverController@OAuthCallback');
Route::get('/clover/oauth', 'CloverController@OAuth');
Route::get('/clover/inventories', 'CloverController@getInventories');
Route::post('/clover/webhooks', 'CloverController@webhooks');
Route::get('/clover/webhooks', 'CloverController@webhooks');
//

Route::post('/wordpress-auth', 'AuthController@wordpressAuth');
Route::get('/', 'SettingsController@index');
Auth::routes();
Route::get('/settings', 'SettingsController@index')->name('settings');
Route::get('/redirect', function () {
    $query = http_build_query([
        'client_id' => 4,
        'redirect_uri' => 'http://127.0.0.1:8001/callback',
        'response_type' => 'code',
        'scope' => '',
    ]);
    return redirect('http://127.0.0.1:8000/oauth/authorize?' . $query);
});

//Route::get('/db-test', function () {
////    DB::table('tbl_w3b_store_api')->limit(1)->get();
//    $customers = DB::table('tbl_customer')->get();
//    foreach ($customers as $customer) {
//        $f = DB::table('tbl_api_users')->select('*')->where('email', $customer->email)->count();
//        if (!$f) {
//            DB::table('tbl_api_users')->insert([
//                'name' => $customer->firstname . ' ' . $customer->lastname,
//                'email' => $customer->email,
//                'password' => Hash::make($customer->real_password),
//            ]);
//        }
//    }
//});
