<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
    /*
     * Auth
     */
    Route::post('/auth/revoke', 'AuthController@revokeToken');
    Route::post('/menu', 'MenuController@activeMenu');
    Route::post('/login', 'AuthController@w3bStoreLogin');
    Route::get('orders/{id}', 'OrderController@getOrdersById');
    Route::get('orders', 'OrderController@getAllOrders');
});
